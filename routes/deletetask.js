var dbPerson = require('../models/person')

var deletetask = (req,res)=>{
    if(!req.body._id){
        res.json({
            success: false,
            msg:"Please enter all details"
        })
    }else{
        dbPerson.findOneAndUpdate({_id:req.body._id},{$set:{status:-1}},(err,data)=>{
            if(err){
                res.json({
                    success:false,
                    msg: "Task not Deleted"
                })
            }else{
                res.json({
                    success:true,
                    msg:"task deleted"
                })
            }
        })
    }
}
module.exports= deletetask