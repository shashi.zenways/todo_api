var dbPerson = require('../models/person')

var addtask = (req, res) => {
    if (!req.body.task) {
        res.json({
            success: false,
            msg: "All data not provided"
        })
    } else {
        var newperson = new dbPerson({
            task: req.body.task,
            status: 0
        })
        newperson.save(( err, data ) => {
            
            if(err) {
                res.json({
                    success: false,
                    msg: err
                })
            }else{
                res.json({
                    success: true, 
                    msg: "Data saved",
                    data: data
                })
            }
        })
    }
}

module.exports = addtask