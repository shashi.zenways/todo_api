var dbPerson = require('../models/person')


var updatetask = (req, res) => {
    if (!req.body.task) {
        res.json({
            success: false,
            msg: "Provide valid data"
        })
    } else {
        console.log(req.body)
        dbPerson.findOneAndUpdate({ _id: req.body._id }, { $set: { task: req.body.task } },
        (err,data)=>{
            if(err){
                res.json({
                    success:false,
                    msg:"Something is worng"
                })
            }else{
                console.log(req.body);
                
                res.json({
                    success:true,
                    msg:"Data updated"
                })
            }
        })
    }
}

module.exports= updatetask