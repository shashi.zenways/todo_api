var dbPerson = require('../models/person')

var alltask = (req, res) => {
    dbPerson.find({status:{$ne :-1}}, (err, data) => {
        if (err) {
            res.json({
                success: false,
                err: err
            })
        } else {
            res.json({
                success: true,
                msg: "All Data",
                data: data
            })
        }
    })
}

module.exports= alltask