var express = require('express')
var app = express.Router()



var addtask = require('./addtask')
app.post('/addtask',addtask)

var alltask = require('./alltask')
app.get('/alltask',alltask)

var updatetask = require('./updatetask')
app.post('/updatetask',updatetask)

var deletetask = require('./deletetask')
app.post('/deletetask',deletetask)


module.exports = app;