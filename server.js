var express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true , useUnifiedTopology: true, useUnifiedTopology: true}))

mongoose.connect('mongodb://localhost/todo', (err, data) => {
    if (err) {
        console.log("Database not connected");

    } else {
        console.log(err);

        console.log("Database is connected");

    }
})

// var newperson = new dbPerson({
//     name:"ABC",
//     age:34,
//     email:"shashi@gmail.com"
// })
// newperson.save((err,data)=>{
//     if(err){
//         console.log("err found",err);
//     }else{
//         console.log("Done.........",data);
//     }
// })

// dbPerson.find({},(err,data)=>{
//     if(err){
//         console.log(err);        
//     }else{
//         console.log(data);        
//     }
// })

var routes = require('./routes/routes')
app.use('/api',routes)


app.listen(3000, () => {
    console.log("server started");

})